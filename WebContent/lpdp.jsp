<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="tugasLpdp.css" rel="stylesheet" type="text/css">
<title>Beasiswa LPDP - Create User</title>
</head>
<body>

<div id="page">

<div id="atas">
	<img alt="" src="header_new.jpg"/>
	<h1>Daftarkan Akun</h1>
	<h4>Catatan : semua isian bertanda * perlu diisi.</h4>
</div>

<div id="tengah" class="form">
	<form action="#">
	
	<div class="form-container">
		<h2>INFORMASI AKUN</h2>
		<table>
			<tr>
				<td>Email <span class="required">*</span></td>
				<td>
					<input type="text" id="email"/>
				</td>
				<td style="color: red; display: none;" id= "td_email">email kosong!!!</td>
			</tr>
			<tr>
				<td>Konfirmasi Email</td>
				<td>
					<input type="text" id="reemail" onchange="cekSama();"/>
					<div class="hint">*Pastikan email anda adalah email pribadi dan aktif</div>
				</td>
				<td style="color: red; display: none;" id= "td_reemail">Konfirmasi email berbeda!!!</td>
			</tr>
			<tr>
				<td>Password <span class="required">*</span></td>
				<td>
					<input type="password" id="password"/>
					<div class="hint">*Password terdiri dari 8-20 karakter, minimal satu huruf kecil, minimal satu huruf besar, minimal satu angka dan minimal satu karakter spesial. Karakter spesial yang dapat digunakan: ! , @ , # , $ atau %</div>
				</td>
				<td style="color: red; display: none;" id="td_password">password kosong!!!</td>
			</tr>
			<tr>
				<td>Konfirmasi Password <span class="required">*</span></td>
				<td><input type="password" id="repassword" onchange="cekSama();"/></td>
				<td style="color: red; display: none;" id="td_repassword">repassword berbeda!!!</td>
			</tr>
		</table>
	</div>
	<br/>
	<br/>
	<div class="form-container">
		<h2>INFORMASI PRIBADI</h2>
		<table>
			<tr>
				<td>
					Nama Lengkap <span class="required">*</span>
					<div class="note">
						Sesuai KTP dan tanpa gelar. <br/>
						Penggunaan huruf kapital <br/>
						<strong>hanya diperkenankan di awal kata</strong><br/> 
						(contoh: Ahmad Budiawan)
					</div>
				</td>
				<td><input type="text" id="nama1"/></td>
				<td style="color: red; display: none;" id= "td_nama1">nama lengkap kosong!!!</td>
			</tr>
			<tr>
				<td>Nama Panggilan <span class="required">*</span></td>
				<td><input type="text" id="nama2"/></td>
				<td style="color: red; display: none;" id= "td_nama2">nama panggilan kosong!!!</td>
			</tr>
			<tr>
				<td>Jenis Kelamin <span class="required">*</span></td>
				<td>
					<select id="jk">
						<option value="laki">Laki - laki</option>
						<option value="perempuan">Perempuan</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Status Menikah <span class="required">*</span></td>
				<td>
					<select id="status">
						<option value="belum">Belum Menikah</option>
						<option value="menikah">Menikah</option>
						<option value="cerai">Duda/Janda/Cerai</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Agama <span class="required">*</span></td>
				<td>
					<select id="agama">
						<option value="islam">Islam</option>
						<option value="protestan">Protestan</option>
						<option value="katolik">Katolik</option>
						<option value="hindu">Hindu</option>
						<option value="buddha">Buddha</option>
						<option value="konghucu">Kong Hu Cu</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Golongan Darah <span class="required">*</span></td>
				<td>
					<select id="golDarah">
						<option value="a">A</option>
						<option value="b">B</option>
						<option value="ab">AB</option>
						<option value="o">O</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Tempat Lahir <span class="required">*</span></td>
				<td><input type="text" id="bplace"/></td>
				<td style="color: red; display: none;" id= "td_bplace">tempat lahir kosong!!!</td>
			</tr>
			<tr>
				<td>
					Tanggal Lahir <span class="required">*</span>
					<div class="note">
						Sesuai KTP
					</div>
				</td>
				<td><input type="date" name="bday"/></td>
			</tr>
			<tr>
				<td>Alamat <span class="required">*</span></td>
				<td><input type="text" id="alamat"/></td>
				<td style="color: red; display: none;" id= "td_alamat">alamat kosong!!!</td>
			</tr>
			<tr>
				<td>Kabupaten/Kota <span class="required">*</span></td>
				<td>
					<select id="kabkota">
						<option value="Pasuruan">Pasuruan</option>
						<option value="Surabaya">Surabaya</option>
						<option value="Sidoarjo">Sidoarjo</option>
						<option value="Malang">Malang</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Kode Pos <span class="required">*</span></td>
				<td><input type="text" id="kodepos"/></td>
				<td style="color: red; display: none;" id= "td_kodepos">kode pos kosong!!!</td>
			</tr>
			<tr>
				<td>
					Handphone <span class="required">*</span>
					<div class="note">
						Harap diisi dengan <br/>
						format menggunakan kode negara. <br/> 
						Untuk Indonesia, gantikan angka <strong>0</strong><br/> 
						di paling depan dengan angka <strong>62</strong>. <br/>
						Contoh: <strong>0</strong>811111111 <br/>
						mohon diisi dengan <strong>62</strong>811111111.
					</div>
				</td>
				<td><input type="text" id="hp"/></td>
				<td style="color: red; display: none;" id= "td_hp">nomor handphone kosong!!!</td>
			</tr>
			<tr>
				<td>Telepon </td>
				<td><input type="text" id="telp"/></td>
			</tr>
			<tr>
				<td>Jenis Pekerjaan <span class="required">*</span></td>
				<td>
					<select id="job">
						<option value="ahli">Ahli Professional</option>
						<option value="pns">PNS</option>
						<option value="abri">TNI / POLRI</option>
						<option value="guru">Guru</option>
						<option value="dosen">Dosen</option>
						<option value="dokter">Dokter</option>
						<option value="fg">Fresh Graduate</option>
						<option value="unemployed">Tidak Bekerja</option>
						<option value="wiraswasta">Wiraswasta</option>
						<option value="lainnya">Lainnya</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Foto(4x6)</td>
				<td>
					<input type="file"/><br/>
				</td>
				<td><div class="hint">Max Upload Size 512KB</div></td>
			</tr>
		</table>
	</div>
	<br/>
	<br/>
	<div class="form-container">
		<h2>PENDIDIKAN TERAKHIR</h2>
		<div class="note">
			Keterangan : Pendidikan Terakhir adalah ijazah pendidikan terakhir yang anda dapat. Jika anda sedang menempuh Program S2/S3, maka pendidikan terakhir anda adalah program yang sedang anda tempuh
		</div>
		<table>
			<tr>
				<td>Jenjang Pendidikan <span class="required">*</span></td>
				<td>
					<select id="pendidikan">
						<option value="s1">S1/D4</option>
						<option value="s2">S2</option>
						<option value="s3">S3</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Tahun Kelulusan <span class="required">*</span></td>
				<td>
				<select id="tahunLulus">
						<option value="1980">1980</option>
						<option value="1981">1981</option>
						<option value="1982">1982</option>
						<option value="1983">1983</option>
						<option value="1984">1984</option>
						<option value="1985">1985</option>
						<option value="1986">1986</option>
						<option value="1987">1987</option>
						<option value="1988">1988</option>
						<option value="1989">1989</option>
						<option value="1990">1990</option>
						<option value="1991">1991</option>
						<option value="1992">1992</option>
						<option value="1993">1993</option>
						<option value="1994">1994</option>
						<option value="1995">1995</option>
						<option value="1996">1996</option>
						<option value="1997">1997</option>
						<option value="1998">1998</option>
						<option value="1999">1999</option>
						<option value="2000">2000</option>
						<option value="2001">2001</option>
						<option value="2002">2002</option>
						<option value="2003">2003</option>
						<option value="2004">2004</option>
						<option value="2005">2005</option>
						<option value="2006">2006</option>
						<option value="2007">2007</option>
						<option value="2008">2008</option>
						<option value="2009">2009</option>
						<option value="2010">2010</option>
						<option value="2011">2011</option>
						<option value="2012">2012</option>
						<option value="2013">2013</option>
						<option value="2014">2014</option>
						<option value="2015">2015</option>
						<option value="2016">2016</option>
						<option value="2017">2017</option>
						<option value="2018">2018</option>
						<option value="2019">2019</option>
						<option value="2020">2020</option>
						<option value="2021">2021</option>
					</select>
					</td>
			</tr>
			<tr>
				<td>Asal Universitas <span class="required">*</span></td>
				<td>
					<select id="asalUniv">
						<option value="dalamNegeri">Dalam Negeri</option>
						<option value="luarNegeri">Luar Negeri</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Perguruan Tinggi <span class="required">*</span></td>
				<td>
					<select id="univ">
						<option value="its">Institut Teknologi Sepuluh Nopember</option>
						<option value="unair">Universitas Airlangga</option>
						<option value="ub">Universitas Brawijaya</option>
						<option value="ui">Universitas Indonesia</option>
						<option value="ugm">Universitas Gajah Mada</option>
						<option value="itb">Institut Teknologi Bandung</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Prodi <span class="required">*</span></td>
				<td>
				<select id="prodi">
					<option value="si">Sistem Informasi</option>
					<option value="if">Teknik Informatika</option>
					<option value="ik">Ilmu Komputer</option>
					<option value="tc">Teknik Komputer</option>
					<option value="te">Teknik Elektro</option>
					<option value="ti">Teknik Industri</option>
					<option value="mb">Manajemen Bisnis</option>
					<option value="mi">Manajemen Informatika</option>
					<option value="ka">Komputerisasi Akuntansi</option>
					<option value="mj">Manajemen</option>
				</select>
				</td>
			</tr>
			<tr>
				<td>IPK Terakhir dengan Format 4.00 <span class="required">*</span></td>
				<td><input type="text" id="ipk"/></td>
				<td style="color: red; display: none;" id= "td_ipk">IPK kosong!!!</td>
			</tr>
		</table>
	</div>
	<br/>
	<br/>
	<div style="text-align: center">
		<button class="btn btn-primary" type="button" onclick="cekIsi(); if (confirm('Apakah anda yakin data akun anda sudah benar?')) {} else {return false}">
			Buat Akun
		</button>
	</div>
	</form>
</div>

<div id="bawah">
	<p>
	Gedung Ali Wardhana Lantai 2,<br/>
	Jalan Lapangan Banteng Timur No. 1, Jakarta 10710<br/>
	Call Center 1500 652<br/>
	Layanan Informasi dan Bantuan LPDP: <a href="#">Layanan Informasi dan Bantuan LPDP</a><br/>
	Situs <a href="#">lpdp.kemenkeu.go.id</a><br/><br/>
	� Lembaga Pengelola Dana Pendidikan 2019<br/>
	jam server: 00:35:24 06-Februari-2019<br/>
	</p>
</div>

</div>

</body>

<script type="text/javascript">
	function cekIsi() {
		var email = document.getElementById("email");
		var td_email = document.getElementById("td_email");
		if (email.value == "") {
			td_email.style.display = "block";
			email.style.borderColor = "red";
		}
		else {
			td_email.style.display = "none";
			email.style.borderColor = "gray";
		}
		
		var nama1 = document.getElementById("nama1");
		var td_nama1 = document.getElementById("td_nama1");
		if (nama1.value == "") {
			td_nama1.style.display = "block";
			nama1.style.borderColor = "red";
		}
		else {
			td_nama1.style.display = "none";
			nama1.style.borderColor = "gray";
		}
		
		var nama2 = document.getElementById("nama2");
		var td_nama2 = document.getElementById("td_nama2");
		if (nama2.value == "") {
			td_nama2.style.display = "block";
			nama2.style.borderColor = "red";
		}
		else {
			td_nama2.style.display = "none";
			nama2.style.borderColor = "gray";
		}
		
		var password = document.getElementById("password");
		var td_password = document.getElementById("td_password");
		if (password.value == "") {
			td_password.style.display = "block";
			password.style.borderColor = "red";
		}
		else {
			td_password.style.display = "none";
			password.style.borderColor = "gray";
		}
		
		var bplace = document.getElementById("bplace");
		var td_bplace = document.getElementById("td_bplace");
		if (bplace.value == "") {
			td_bplace.style.display = "block";
			bplace.style.borderColor = "red";
		}
		else {
			td_bplace.style.display = "none";
			bplace.style.borderColor = "gray";
		}
		
		var alamat = document.getElementById("alamat");
		var td_alamat = document.getElementById("td_alamat");
		if (alamat.value == "") {
			td_alamat.style.display = "block";
			alamat.style.borderColor = "red";
		}
		else {
			td_alamat.style.display = "none";
			alamat.style.borderColor = "gray";
		}
		
		var kodepos = document.getElementById("kodepos");
		var td_kodepos = document.getElementById("td_kodepos");
		if (kodepos.value == "") {
			td_kodepos.style.display = "block";
			kodepos.style.borderColor = "red";
		}
		else {
			td_kodepos.style.display = "none";
			kodepos.style.borderColor = "gray";
		}
		
		var hp = document.getElementById("hp");
		var td_hp = document.getElementById("td_hp");
		if (hp.value == "") {
			td_hp.style.display = "block";
			hp.style.borderColor = "red";
		}
		else {
			td_hp.style.display = "none";
			hp.style.borderColor = "gray";
		}
		
		var ipk = document.getElementById("ipk");
		var td_ipk = document.getElementById("td_ipk");
		if (ipk.value == "") {
			td_ipk.style.display = "block";
			ipk.style.borderColor = "red";
		}
		else {
			td_ipk.style.display = "none";
			ipk.style.borderColor = "gray";
		}
	}
	
	function cekSama(){
		var password = document.getElementById("password");
		var repassword = document.getElementById("repassword");
		var td_repassword = document.getElementById("td_repassword");
		
		if (password.value!=repassword.value) {
			td_repassword.style.display = "block";
			repassword.style.borderColor = "red";
		} else {
			td_repassword.style.display = "none";
			repassword.style.borderColor = "gray";
		}
		
		var email = document.getElementById("email");
		var reemail = document.getElementById("reemail");
		var td_reemail = document.getElementById("td_reemail");
		
		if (email.value!=reemail.value) {
			td_reemail.style.display = "block";
			reemail.style.borderColor = "red";
		} else {
			td_reemail.style.display = "none";
			reemail.style.borderColor = "gray";
		}
	}
	
</script>
</html>
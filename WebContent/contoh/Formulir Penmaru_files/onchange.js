﻿$(function () {
    $.ajax({
        type: "GET",
        url: "/Formulir/Negara",
        datatype: "Json",
        success: function (data) {
            $.each(data, function (index, value) {
                $('#Neg_Lahir').append('<option value="' + value.COUNTRY_CODE + '">' + value.NAME_OF_COUNTRY + '</option>');
            });
        }
    });
    $('#Neg_Lahir').change(function () {
        $('#Prop_Lahir').empty();
        $('#Prop_Lahir').append($('<option/>', {
            value: 0,
            text: "Select Provinsi"
        }));
        $.ajax({
            type: "POST",
            url: "/Formulir/provinsi",
            datatype: "Json",
            data: { contrycode: $('#Neg_Lahir').val() },
            success: function (data) {
                $.each(data, function (index, value) {
                    $('#Prop_Lahir').append('<option value="' + value.PROVINCE_CODE + '">' + value.NAME_OF_PROVINCE + '</option>');
                    $('#Prop_Lahir').val($('#Prop_Lahir option:first').val());
                });
            }
        });
    });
    $('#Prop_Lahir, #Neg_Lahir').change(function () {
        $('#Kab_Lahir').empty();
        $('#Kab_Lahir').append($('<option/>', {
            value: 0,
            text: "Select Kabupaten Lahir"
        }));
        $.ajax({
            type: "POST",
            url: "/Formulir/kabupaten",
            datatype: "Json",
            data: { provinid: $('#Prop_Lahir').val(), contrycode: $('#Neg_Lahir').val() },
            success: function (data) {
                $.each(data, function (index, value) {
                    $('#Kab_Lahir').append('<option value="' + value.KODE_KOTA + '">' + value.NAMA_KOTA + '</option>');
                    $('#Kab_Lahir').val($('#Prop_Lahir option:first').val());
                });
            }
        });
    });
});
///asal
$(function () {
    $.ajax({
        type: "GET",
        url: "/Formulir/Negara",
        datatype: "Json",
        success: function (data) {
            $.each(data, function (index, value) {
                $('#Country_Code').append('<option value="' + value.COUNTRY_CODE + '">' + value.NAME_OF_COUNTRY + '</option>');
            });
        }
    });
    $('#Country_Code').change(function () {
        $('#Province_Code').empty();
        $('#Province_Code').append($('<option/>', {
            value: 0,
            text: "Select Provinsi Asal"
        }));
        $.ajax({
            type: "POST",
            url: "/Formulir/provinsi",
            datatype: "Json",
            data: { contrycode: $('#Country_Code').val() },
            success: function (data) {
                $.each(data, function (index, value) {
                    $('#Province_Code').append('<option value="' + value.PROVINCE_CODE + '">' + value.NAME_OF_PROVINCE + '</option>');
                    $('#Province_Code').val($('#Prop_Lahir option:first').val());
                });
            }
        });
    });
    $('#Province_Code, #Country_Code').change(function () {
        $('#City_Code').empty();
        $('#City_Code').append($('<option/>', {
            value: 0,
            text: "Select Kota Asal"
        }));
        $.ajax({
            type: "POST",
            url: "/Formulir/kabupaten",
            datatype: "Json",
            data: { provinid: $('#Province_Code').val(), contrycode: $('#Country_Code').val() },
            success: function (data) {
                $.each(data, function (index, value) {
                    $('#City_Code').append('<option value="' + value.KODE_KOTA + '">' + value.NAMA_KOTA + '</option>');
                    $('#City_Code').val($('#Prop_Lahir option:first').val());
                });
            }
        });
    });
    $('#City_Code, #Province_Code, #Country_Code').change(function () {
        $('#Kecamatan_Code').empty();
        $('#Kecamatan_Code').append($('<option/>', {
            value: 0,
            text: "Select Kecamatan Asal"
        }));
        $.ajax({
            type: "POST",
            url: "/Formulir/kecamatan",
            datatype: "Json",
            data: { kodekota: $('#City_Code').val(), provinid: $('#Province_Code').val(), contrycode: $('#Country_Code').val() },
            success: function (data) {
                $.each(data, function (index, value) {
                    $('#Kecamatan_Code').append('<option value="' + value.KODE_KECAMATAN + '">' + value.NAMA_KECAMATAN + '</option>');
                    $('#Kecamatan_Code').val($('#Prop_Lahir option:first').val());
                });
            }
        });
    });
});
//education
$(function () {
    $.ajax({
        type: "GET",
        url: "/Formulir/Thjaran",
        datatype: "Json",
        success: function (data) {
            $.each(data, function (index, value) {
                $('#Thajaranid').append('<option value="' + value.THAJARANID + '">' + value.THAJARANID + '</option>');
            });
        }
    });
    $('#Thajaranid').change(function () {
        $('#Entry_Period_Id').empty();
        $.ajax({
            type: "POST",
            url: "/Formulir/kecamatan",
            datatype: "Json",
            data: { thjaran: $('#Thajaranid').val() },
            success: function (data) {
                $.each(data, function (index, value) {
                    $('#Entry_Period_Id').append('<option value="' + value.ENTRY_PERIOD_ID + '">' + value.ENTRY_PERIOD_ID + '</option>');
                });
            }
        });
    });
    $('#Thajaranid, #Entry_Period_Id').change(function () {
        $('#Education_Group_Id').empty();
        $.ajax({
            type: "POST",
            url: "/Formulir/education",
            datatype: "Json",
            data: { thjaran: $('#Thajaranid').val(), period: $('#Entry_Period_Id').val() },
            success: function (data) {
                $.each(data, function (index, value) {
                    $('#Education_Group_Id').append('<option value="' + value.EDUCATION_GROUP_ID + '">' + value.NAME_OF_GROUP + '</option>');
                });
            }
        });
    });
    $('#Thajaranid, #Entry_Period_Id, #Education_Group_Id').change(function () {
        $('#fee').empty();
        $.ajax({
            type: "POST",
            url: "/Formulir/fee",
            datatype: "Json",
            data: { thjaran: $('#Thajaranid').val(), period: $('#Entry_Period_Id').val(), education: $('#Education_Group_Id').val() },
            success: function (data) {
                $.each(data, function (index, value) {
                    $('#fee').append('<option value="' + value.EDUCATION_GROUP_ID + '">' + value.FEE_RP + '</option>');
                });
            }
        });
    });
    $('#Thajaranid, #Entry_Period_Id, #Education_Group_Id').change(function () {
        $('#Prodi_Id_Pil1').empty();
        $.ajax({
            type: "POST",
            url: "/Formulir/prodi",
            datatype: "Json",
            data: { thjaran: $('#Thajaranid').val(), period: $('#Entry_Period_Id').val(), education: $('#Education_Group_Id').val() },
            success: function (data) {
                $.each(data, function (index, value) {
                    $('#Prodi_Id_Pil1').append('<option value="' + value.AVAILABLE_EDU_ID + '">' + value.NAME_OF_AVAILABLE_PROGRAM + '</option>');
                });
            }
        });
    });
    $('#Thajaranid, #Entry_Period_Id, #Education_Group_Id, #Prodi_Id_Pil1').change(function () {
        $('#Classprog_Id_Pil1').empty();
        $.ajax({
            type: "POST",
            url: "/Formulir/classprogram",
            datatype: "Json",
            data: { thjaran: $('#Thajaranid').val(), period: $('#Entry_Period_Id').val(), education: $('#Education_Group_Id').val(), prodiid: $('#Prodi_Id_Pil1').val() },
            success: function (data) {
                $.each(data, function (index, value) {
                    $('#Classprog_Id_Pil1').append('<option value="' + value.classProg_id + '">' + value.name_of_class_program + '</option>');
                });
            }
        });
    });
    $('#Thajaranid, #Entry_Period_Id, #Education_Group_Id').change(function () {
        $('#Prodi_Id_Pil2').empty();
        $.ajax({
            type: "POST",
            url: "/Formulir/prodi",
            datatype: "Json",
            data: { thjaran: $('#Thajaranid').val(), period: $('#Entry_Period_Id').val(), education: $('#Education_Group_Id').val() },
            success: function (data) {
                $.each(data, function (index, value) {
                    $('#Prodi_Id_Pil2').append('<option value="' + value.AVAILABLE_EDU_ID + '">' + value.NAME_OF_AVAILABLE_PROGRAM + '</option>');
                });
            }
        });
    });
    $('#Thajaranid, #Entry_Period_Id, #Education_Group_Id, #Prodi_Id_Pil2').change(function () {
        $('#Classprog_Id_Pil2').empty();
        $.ajax({
            type: "POST",
            url: "/Formulir/classprogram",
            datatype: "Json",
            data: { thjaran: $('#Thajaranid').val(), period: $('#Entry_Period_Id').val(), education: $('#Education_Group_Id').val(), prodiid: $('#Prodi_Id_Pil2').val() },
            success: function (data) {
                $.each(data, function (index, value) {
                    $('#Classprog_Id_Pil2').append('<option value="' + value.classProg_id + '">' + value.name_of_class_program + '</option>');
                });
            }
        });
    });
});
//kotasma
$(function () {
    $.ajax({
        type: "GET",
        url: "/Formulir/Negara",
        datatype: "Json",
        success: function (data) {
            $.each(data, function (index, value) {
                $('#kodenegara').append('<option value="' + value.COUNTRY_CODE + '">' + value.NAME_OF_COUNTRY + '</option>');
            });
        }
    });
    $('#kodenegara').change(function () {
        if (this.value >= '1') {
            $("#kodenegaras").hide();
            $("#propsltas").show();
        }
        else {
            $("#kodenegaras").show();
        }
    });
    $('#kodenegara').change(function () {
        $('#propslta').empty();
        $.ajax({
            type: "POST",
            url: "/Formulir/provinsi",
            datatype: "Json",
            data: { contrycode: $('#kodenegara').val() },
            success: function (data) {
                $.each(data, function (index, value) {
                    $('#propslta').append('<option value="' + value.PROVINCE_CODE + '">' + value.NAME_OF_PROVINCE + '</option>');
                });
            }
        });
    });
    $('#propslta').change(function () {
        if (this.value != null) {
            $("#propsltas").hide();
            $("#asalaslta").show();
        }
        else {
            $("#propsltas").show();
        }
    });
    $('#kodenegara, #propslta').change(function () {
        $('#Asal_Slta_id').empty();
        $.ajax({
            type: "POST",
            url: "/Formulir/Sma",
            datatype: "Json",
            data: { contrycode: $('#kodenegara').val(), provinid: $('#propslta').val() },
            success: function (data) {
                $.each(data, function (index, value) {
                    $('#Asal_Slta_id').append('<option value="' + value.INSTITUTION_CODE + '">' + value.NAME + '</option>');
                });
            }
        });
    });
    $('#resets').bind("click", function () {
            $("#propsltas").hide();
            $("#asalaslta").hide();
            $("#kodenegaras").show();
            $('#kodenegara')[0].selectedIndex = 0;
            $('#propslta')[0].selectedIndex = 0;
            $('#Asal_Slta_id')[0].selectedIndex = 0;
    });
    $('#Asal_Slta_id').change(function () {
        $('#Nama_Slta').empty();
        $.ajax({
            type: "POST",
            url: "/Formulir/namasma",
            datatype: "Json",
            data: { institute: $('#Asal_Slta_id').val() },
            success: function (data) {
                $.each(data, function (index, value) {
                    $('#Nama_Slta').append('<input type="text" class="form-control" hidden name="Nama_Slta" value="' + value.NAME + '" required/><input type="text" class="form-control" disabled value="' + value.NAME + '" required/>');
                });
            }
        });
    });
    $('#Asal_Slta_id').change(function () {
        $('#Alamat_Slta').empty();
        $.ajax({
            type: "POST",
            url: "/Formulir/alamatsma",
            datatype: "Json",
            data: { institute: $('#Asal_Slta_id').val() },
            success: function (data) {
                $.each(data, function (index, value) {
                    $('#Alamat_Slta').append('<input type="text" class="form-control" hidden name="Alamat_Slta" value="' + value.ADRESS_INSTITUTION + '" required/><input type="text" class="form-control" disabled value="' + value.ADRESS_INSTITUTION + '" required/>');
                });
            }
        });
    });
    $('#Asal_Slta_id').change(function () {
        $('#Neg_Slta').empty();
        $.ajax({
            type: "POST",
            url: "/Formulir/negarasma",
            datatype: "Json",
            data: { institute: $('#Asal_Slta_id').val() },
            success: function (data) {
                $.each(data, function (index, value) {
                    $('#Neg_Slta').append('<input type="text" class="form-control" name="Neg_Slta" hidden value="' + value.COUNTRY_CODE + '" required/><input type="text" disabled class="form-control" value="' + value.NAME_OF_COUNTRY + '" required/>');
                });
            }
        });
    });
    $('#Asal_Slta_id').change(function () {
        $('#Prop_Slta').empty();
        $.ajax({
            type: "POST",
            url: "/Formulir/provinsisma",
            datatype: "Json",
            data: { institute: $('#Asal_Slta_id').val() },
            success: function (data) {
                $.each(data, function (index, value) {
                    $('#Prop_Slta').append('<input type="text" class="form-control" hidden name="Prop_Slta" value="' + value.PROVINCE_CODE + '" required/><input type="text" disabled class="form-control" value="' + value.NAME_OF_PROVINCE + '" required/>');
                });
            }
        });
    });
    $('#Asal_Slta_id').change(function () {
        $('#Kab_Slta').empty();
        $.ajax({
            type: "POST",
            url: "/Formulir/kotasma",
            datatype: "Json",
            data: { institute: $('#Asal_Slta_id').val() },
            success: function (data) {
                $.each(data, function (index, value) {
                    $('#Kab_Slta').append('<input type="text" hidden class="form-control" name="Kab_Slta" value="' + value.KODE_KOTA + '" required/><input type="text" disabled class="form-control" value="' + value.NAMA_KOTA + '" required/>');
                });
            }
        });
    });
});

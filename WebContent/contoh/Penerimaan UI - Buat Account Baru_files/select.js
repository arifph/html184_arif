function selectLoad(sel, target, url) {
	var ajax = new sack();
	ajax.requestFile = url + '?id=' + sel.options[sel.selectedIndex].value;
	ajax.method = 'get';
	var clearSelect = function(el) {
		for (var i = el.childNodes.length - 1; i >= 0; i--) {
			el.removeChild(el.childNodes[i]);
		}
	};
	ajax.onLoading = function() {
		var tsel = document.getElementById(target);
		clearSelect(tsel);
		var opt = document.createElement('option');
		opt.value = 0;
		opt.appendChild(document.createTextNode('Loading data...'));
		tsel.appendChild(opt);
	};
	ajax.onCompletion = function() {
		var tsel = document.getElementById(target);
		// in opera, onloading doesn't run
		clearSelect(tsel);
		try {
			var data = eval('(' + ajax.response + ')');
		} catch (e) {
			//alert('Data error: '+ajax.response);
			return;
		}
		var opt;
		for (var i = 0; i < data.length; i++) {
			opt = document.createElement('option');
			opt.value = data[i][0];
			opt.appendChild(document.createTextNode(data[i][1]));
			tsel.appendChild(opt);
		}
	};
	ajax.runAJAX();
}

function login_digest() {
  var p = $("#login_p");
  if (p && p.val().length) {
    var d = $("#login_d");
    $.get('/id/ajax/challenge', function(c) {
      d.val(Crypto.SHA1(c + Crypto.SHA1(p.val())));
      p.val("");
      $("#login_form").submit();
    });
    return false;
  }
  return true;
}

function embed_csrf(token) {
  $('form').each(function() {
    if ($(this).find('input[name="csrf_token"]').length == 0) {
      $(this).append('<input type="hidden" name="csrf_token" value="' + token + '" />');
    }
  });
}

$(function() {
  $("form input").each(function(i, el) {
    var cls = el.type == "submit" ? "submit_btn" : el.type;
    if (! $(el).hasClass(cls)) $(el).addClass(cls);
    el.onfocus = function() { $(this).addClass("focus"); }
    el.onblur = function() { $(this).removeClass("focus"); };
  });

  function bgfileRefresh(fileurl) {
    var count = 0;
    $.ajax({
      method: 'HEAD',
      type: 'HEAD', // jquery <1.9.0
      url: fileurl
    }).done(function(data) {
      $('#bgfile-download').empty().html('<strong>Klik untuk mengunduh</strong> ').append($('<a/>').css('font-weight', 'bold').attr('href', fileurl).text(fileurl.split('/').slice(-1)[0]));
    }).fail(function() {
      if (++count < 600) {
        setTimeout(function() { bgfileRefresh(fileurl); }, 1000);
      } else {
        alert('Maaf, file tidak dapat diunduh, silakan mencoba kembali dengan menekan tombol Back dan mengklik tombol/link sebelumnya.');
      }
    });
  }

  $('body').on('click', 'a.bgfile', function() {
    var self = $(this);
    var modal = new tingle.modal({
      closeMethods: ['overlay', 'button', 'escape'],
      onOpen: function() {
        var url = self.attr('href');
        $.getJSON(url, function(data) {
          console.log(data);
          if (data.filename) {
            bgfileRefresh(data.filename);
          }
        }).fail(function() {
          $('#bgfile-download').empty().text('Maaf, file tidak dapat diunduh saat ini. Silakan mencoba beberapa saat lagi.');
        });
      }
    });
    modal.setContent('<div id="bgfile-download"><strong>Generating PDF file...</strong><span class="spinner"></span></div>');
    modal.open();

    return false;
  });
});

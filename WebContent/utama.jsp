<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>JUDUL ATAS</title>
</head>
<body>
	<a href="../html184/tes.jsp">page tes</a> <br/><br/>

	Arif
	enter<br/> br <br/> <br/> br 2x
	<br/>
	<h1>h1</h1>
	<h2>h2</h2>
	<h3>h3</h3>
	<h4>h4</h4>
	<h5>h5</h5>
	<a style = "color: red; font-family: fantasy; font-size: s-large;"> Custom </a>
	<br/>
	<b>bold</b> <i>italic</i> <u>underline</u>
	<br/>
	<br/>
	<table border="1">
		<tr style="color: red; background: yellow;">
			<td>A</td>
			<td>B</td>
		</tr>
		<tr>
			<td>C</td>
			<td>D</td>
		</tr>
	</table>
	<br/>
	<table border="1">
		<tr>
			<td style="color: red; background: yellow;">A</td>
			<td>B</td>
		</tr>
		<tr>
			<td>C</td>
			<td>D</td>
		</tr>
	</table>
	<br/>
	<table border="5" style="color: red; background: yellow;">
		<tr>
			<td>A</td>
			<td>B</td>
		</tr>
		<tr>
			<td>C</td>
			<td>D</td>
		</tr>
	</table>
	<br/>
	<table border="0">
		<tr>
			<td style="color: red; background: yellow;">A</td>
			<td>B</td>
		</tr>
		<tr>
			<td>C</td>
			<td>D</td>
		</tr>
	</table>
	<br/>
	FORM
	<br/>
	<form action="#">
		txt<input type = "text"/> <br/>
		txt size<input type="text" size="5"/> <br/>
		txt maxlength<input type="text" maxlength="5"/> <br/>
		txt placeholder<input type="text" placeholder="bayang2"/> <br/>
		txt style<input type="text" style="color: red; background-color: yellow;"/> <br/>
		password<input type="password" /> <br/>
		file<input type="file"/><br/>
		hidden<input type="hidden"/><br/>
		radio<input type="radio" name="sama"/> Satu <input type="radio" name="sama"/> Dua<br/>
		select <select>
			<option>Satu</option>
			<option>Dua</option>
			<option>Tiga</option>
			<option>Empat</option>
		</select>
		<a href="#">link</a>
		<img alt="" src="oracle HR schema.png">
	</form>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="tugasUmy.css" rel="stylesheet" type="text/css">
<title>Formulir Penmaru</title>
</head>
<body style="background-image: url(background_penmaru_umy.png);">
<div id="tengah">
	<form action="#">
	
	<h1>Formulir Pendaftaran UMY</h1>
	<div>
		<label class="form-label">Nama:</label>
		<input type="text" id="nama" class="form-control"/>
		<div style="color: red; display:none;" id= "alert_nama">nama kosong!!!</div>
	</div>
	<div>
		<label class="form-label">No. KTP/NIK:</label>
		<input type="text" id="ktp" class="form-control"/>
		<div style="color: red; display:none;" id= "alert_ktp">nomor KTP kosong!!!</div>
	</div>
	<div>
		<label class="form-label">Jenis Kelamin:</label><br/>
		<input type="radio" name="jk"/><div class="form-radio">Laki-laki</div> <br/>
		<input type="radio" name="jk"/><div class="form-radio">Perempuan</div>
	</div>
	<div>
		<label class="form-label">Agama:</label><br/>
		<input type="radio" name="agama"/><div class="form-radio">Islam</div> <br/>
		<input type="radio" name="agama"/><div class="form-radio">Kristen</div> <br/>
		<input type="radio" name="agama"/><div class="form-radio">Katolik</div> <br/>
		<input type="radio" name="agama"/><div class="form-radio">Hindu</div> <br/>
		<input type="radio" name="agama"/><div class="form-radio">Budha</div> <br/>
		<input type="radio" name="agama"/><div class="form-radio">Kong Hu Cu</div> <br/>
	</div>
	<div>
		<label class="form-label">Tanggal Lahir:</label><br/>
		<input type="date" name="bday"/><br/>
		<b>format: bulan/tanggal/tahun</b>
	</div>
	<div>
		<label class="form-label">Tempat Lahir:</label>
		<input type="text" id="bplace" class="form-control"/>
		<div style="color: red; display:none;" id= "alert_bplace">tempat lahir kosong!!!</div>
	</div>
	<div>
		<label class="form-label">Negara Lahir:</label><br/>
		<select id="negaraLahir">
			<option value="Indonesia">Indonesia</option>
		</select>
	</div>
	<div>
		<label class="form-label">Provinsi Lahir:</label><br/>
		<select id="negaraLahir">
			<option value="Jatim">Jawa Timur</option>
		</select>
	</div>
	<div>
		<label class="form-label">Kota/Kabupaten Kelahiran:</label><br/>
		<select id="kabKotaLahir">
			<option value="Pasuruan">Pasuruan</option>
			<option value="Surabaya">Surabaya</option>
			<option value="Sidoarjo">Sidoarjo</option>
			<option value="Malang">Malang</option>
		</select>
	</div>
	<div>
		<label class="form-label">Alamat Asal:</label>
		<input type="text" id="alamat" class="form-control"/>
		<div style="color: red; display:none;" id= "alert_alamat">alamat kosong!!!</div>
	</div>
	<div>
		<label class="form-label">Negara Asal:</label><br/>
		<select id="negaraAsal">
			<option value="Indonesia">Indonesia</option>
		</select>
	</div>
	<div>
		<label class="form-label">Provinsi Asal:</label><br/>
		<select id="negaraAsal">
			<option value="Jatim">Jawa Timur</option>
		</select>
	</div>
	<div>
		<label class="form-label">Kota/Kabupaten Asal:</label><br/>
		<select id="kabKotaAsal">
			<option value="Pasuruan">Pasuruan</option>
			<option value="Surabaya">Surabaya</option>
			<option value="Sidoarjo">Sidoarjo</option>
			<option value="Malang">Malang</option>
		</select>
	</div>
	<div>
		<label class="form-label">Kecamatan Asal:</label>
		<input type="text" id="kec" class="form-control"/>
		<div style="color: red; display:none;" id= "alert_kec">kecamatan asal kosong!!!</div>
	</div>
	<div>
		<label class="form-label">Kelurahan Asal:</label>
		<input type="text" id="kel" class="form-control"/>
		<div style="color: red; display:none;" id= "alert_kel">kelurahan asal kosong!!!</div>
	</div>
	<div>
		<label class="form-label">Kode Pos Asal:</label>
		<input type="text" id="kodePos" class="form-control"/>
		<div style="color: red; display:none;" id= "alert_kodePos">kode pos kosong!!!</div>
	</div>
	<div>
		<label class="form-label">Email:</label>
		<input type="text" id="email" class="form-control"/>
		<div style="color: red; display:none;" id= "alert_email">email kosong!!!</div>
	</div>
	<div>
		<label class="form-label">No. Handphone:</label>
		<input type="text" id="noHP" class="form-control"/>
		<div style="color: red; display:none;" id= "alert_noHP">nomor handphone kosong!!!</div>
	</div>
	<div>
		<label class="form-label">IPA/IPS/IPC:</label><br/>
		<select id="kelompok">
			<option value="ipa">Kelompok IPA</option>
			<option value="ips">Kelompok IPS</option>
			<option value="ipc">Kelompok IPC</option>
		</select>
	</div>
	<div>
		<label class="form-label">Harga:</label>
		<input type="text" id="harga" class="form-control" style="background-color:gray;" value="dirahasiakan" disabled/>
	</div>
	<div>
		<label class="form-label">Prodi Pilihan 1:</label><br/>
		<select id="prodi1">
			<option value="si">Sistem Informasi</option>
			<option value="if">Teknik Informatika</option>
			<option value="ik">Ilmu Komputer</option>
			<option value="tc">Teknik Komputer</option>
			<option value="te">Teknik Elektro</option>
			<option value="ti">Teknik Industri</option>
			<option value="mb">Manajemen Bisnis</option>
			<option value="mi">Manajemen Informatika</option>
			<option value="ka">Komputerisasi Akuntansi</option>
			<option value="mj">Manajemen</option>
		</select>
	</div>
	<div>
		<label class="form-label">Kelas Program Pilihan 1:</label><br/>
		<select id="kelas1">
			<option value="reguler">Reguler</option>
		</select>
	</div>
	<div>
		<label class="form-label">Prodi Pilihan 2:</label><br/>
		<select id="prodi2">
			<option value="si">Sistem Informasi</option>
			<option value="if">Teknik Informatika</option>
			<option value="ik">Ilmu Komputer</option>
			<option value="tc">Teknik Komputer</option>
			<option value="te">Teknik Elektro</option>
			<option value="ti">Teknik Industri</option>
			<option value="mb">Manajemen Bisnis</option>
			<option value="mi">Manajemen Informatika</option>
			<option value="ka">Komputerisasi Akuntansi</option>
			<option value="mj">Manajemen</option>
		</select>
	</div>
	<div>
		<label class="form-label">Kelas Program Pilihan 2:</label><br/>
		<select id="kelas2">
			<option value="reguler">Reguler</option>
		</select>
	</div>
	<div>
		<label class="form-label">Negara Asal SMA:</label><br/>
		<select id="negaraAsalSMA">
			<option value="Indonesia">Indonesia</option>
		</select>
	</div>
	<div>
		<label class="form-label">Nama SMA:</label>
		<input type="text" id="namaSMA" class="form-control" style="background-color:gray;" value="" disabled/>
	</div>
	<div>
		<label class="form-label">Alamat Asal SMA:</label>
		<input type="text" id="alamatSMA" class="form-control" style="background-color:gray;" value="" disabled/>
	</div>
	<div>
		<label class="form-label">Negara Asal SMA:</label>
		<input type="text" id="negaraAsalSMA" class="form-control" style="background-color:gray;" value="" disabled/>
	</div>
	<div>
		<label class="form-label">Provinsi Asal SMA:</label>
		<input type="text" id="provinsiAsalSMA" class="form-control" style="background-color:gray;" value="" disabled/>
	</div>
	<div>
		<label class="form-label">Kabupaten/Kota Asal SMA:</label>
		<input type="text" id="kabKotaAsalSMA" class="form-control" value="" style="background-color:gray;" disabled/>
	</div>
	<div>
		<label class="form-label">Jurusan SMA:</label><br/>
		<select id="jurusanSMA">
			<option value="ipa">IPA</option>
			<option value="ips">IPS</option>
			<option value="bahasa">Bahasa</option>
			<option value="tidakAda">Tidak Ada</option>
		</select>
	</div>
	<div style="text-align: left">
		<button class="btn btn-primary" type="button" onclick="cekIsi();">
			simpan
		</button>
	</div>
	</form>
</div>

</body>

<script type="text/javascript">
	function cekIsi() {
		var nama = document.getElementById("nama");
		var alert_nama = document.getElementById("alert_nama");
		if (nama.value == "") {
			alert_nama.style.display = "block";
			nama.style.borderColor = "red";
		}
		else {
			alert_nama.style.display = "none";
			nama.style.borderColor = "gray";
		}
		
		var ktp = document.getElementById("ktp");
		var alert_ktp = document.getElementById("alert_ktp");
		if (ktp.value == "") {
			alert_ktp.style.display = "block";
			ktp.style.borderColor = "red";
		}
		else {
			alert_ktp.style.display = "none";
			ktp.style.borderColor = "gray";
		}
		
		var bplace = document.getElementById("bplace");
		var alert_bplace = document.getElementById("alert_bplace");
		if (bplace.value == "") {
			alert_bplace.style.display = "block";
			bplace.style.borderColor = "red";
		}
		else {
			alert_bplace.style.display = "none";
			bplace.style.borderColor = "gray";
		}
		
		var alamat = document.getElementById("alamat");
		var alert_alamat = document.getElementById("alert_alamat");
		if (alamat.value == "") {
			alert_alamat.style.display = "block";
			alamat.style.borderColor = "red";
		}
		else {
			alert_alamat.style.display = "none";
			alamat.style.borderColor = "gray";
		}
		
		var kec = document.getElementById("kec");
		var alert_kec = document.getElementById("alert_kec");
		if (kec.value == "") {
			alert_kec.style.display = "block";
			kec.style.borderColor = "red";
		}
		else {
			alert_kec.style.display = "none";
			kec.style.borderColor = "gray";
		}
		
		var kel = document.getElementById("kel");
		var alert_kel = document.getElementById("alert_kel");
		if (kel.value == "") {
			alert_kel.style.display = "block";
			kel.style.borderColor = "red";
		}
		else {
			alert_kel.style.display = "none";
			kel.style.borderColor = "gray";
		}
		
		var kodePos = document.getElementById("kodePos");
		var alert_kodePos = document.getElementById("alert_kodePos");
		if (kodePos.value == "") {
			alert_kodePos.style.display = "block";
			kodePos.style.borderColor = "red";
		}
		else {
			alert_kodePos.style.display = "none";
			kodePos.style.borderColor = "gray";
		}
		
		var noHP = document.getElementById("noHP");
		var alert_noHP = document.getElementById("alert_noHP");
		if (noHP.value == "") {
			alert_noHP.style.display = "block";
			noHP.style.borderColor = "red";
		}
		else {
			alert_noHP.style.display = "none";
			noHP.style.borderColor = "gray";
		}
		
		var email = document.getElementById("email");
		var alert_email = document.getElementById("alert_email");
		if (email.value == "") {
			alert_email.style.display = "block";
			email.style.borderColor = "red";
		}
		else {
			alert_email.style.display = "none";
			email.style.borderColor = "gray";
		}
	}
	
	/* function cekSama(){
		var password = document.getElementById("password");
		var repassword = document.getElementById("repassword");
		var td_repassword = document.getElementById("td_repassword");
		
		if (password.value!=repassword.value) {
			td_repassword.style.display = "block";
			repassword.style.borderColor = "red";
		} else {
			td_repassword.style.display = "none";
			repassword.style.borderColor = "gray";
		}
	} */
	
</script>
</html>
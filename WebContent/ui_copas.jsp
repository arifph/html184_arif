<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="tugas.css" rel="stylesheet" type="text/css">
<link href="styleUI.css" rel="stylesheet" type="text/css">
<title>Penerimaan UI - Buat Akun Baru</title>
</head>
<body>

<div id="wrapper">
	<div class="sitetabs">
		<ol>
			<li><a href="http://simak.ui.ac.id/">Portal Informasi SIMAK UI</a></li>
			<li class="current"><a href="http://penerimaan.ui.ac.id/">Pendaftaran SIMAK UI</a></li>
			<li><a href="http://beasiswa.ui.ac.id/">Informasi Beasiswa</a></li>
			<li><a href="https://admission.ui.ac.id/?1549174108">International Students Admission</a></li>
		</ol>
		<div style="clear:both"></div>
	</div>
</div>
	
<div id="atas">
	<h2>Buat akun baru</h2>
</div>

<div id="tengah">
	<form action="#">
	
	<h2>Login</h2>
	<table>
		<tr>
			<td>NAMA</td>
			<td><input type="text" id="nama"/></td>
			<td style="color: red; display: none;" id= "td_nama">nama kosong!!!</td>
		</tr>
		<tr>
			<td>PASSWORD</td>
			<td><input type="password" id="password"/></td>
			<td style="color: red; display: none;" id="td_password">password kosong!!!</td>
		</tr>
		<tr>
			<td>REPASSWORD</td>
			<td><input type="password" id="repassword" onchange="cekSama();"/></td>
			<td style="color: red; display: none;" id="td_repassword">repassword berbeda!!!</td>
		</tr>
		</table>
		
		<h2>Identitas</h2>
		<table>
		
		</table>

		<h2>Kontak</h2>
		<table>
		
		</table>

		<table>
			<tr>
				<td><button type="reset">Reset</button></td>
				<td><button type="button" onclick="cekIsi();">Tekan</button></td>
			</tr>
		</table>
	</form>
</div>

</body>

<script type="text/javascript">
	function cekIsi() {
		var nama = document.getElementById("nama");
		var td_nama = document.getElementById("td_nama");
		if (nama.value == "") {
			td_nama.style.display = "block";
			nama.style.borderColor = "red";
		}
		else {
			td_nama.style.display = "none";
			nama.style.borderColor = "gray";
		}
		
		var password = document.getElementById("password");
		var td_password = document.getElementById("td_password");
		if (password.value == "") {
			td_password.style.display = "block";
			password.style.borderColor = "red";
		}
		else {
			td_password.style.display = "none";
			password.style.borderColor = "gray";
		}
	}
	
	function cekSama(){
		var password = document.getElementById("password");
		var repassword = document.getElementById("repassword");
		var td_repassword = document.getElementById("td_repassword");
		
		if (password.value!=repassword.value) {
			td_repassword.style.display = "block";
			repassword.style.borderColor = "red";
		} else {
			td_repassword.style.display = "none";
			repassword.style.borderColor = "gray";
		}
	}
	
	function pilihNomor(){
		var provider = document.getElementById("provider");
		var hp = document.getElementById("hp");
		
		hp.value = provider.value;
	}
	
	function pilihProvider(){
		var provider = document.getElementById("provider");
		var hp = document.getElementById("hp");
		provider.value = hp.value;
	}
	
</script>
</html>
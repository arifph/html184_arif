<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="tugas.css" rel="stylesheet" type="text/css">
<title>Penerimaan UI - Buat Akun Baru</title>
</head>
<body>
	
<div id="atas">
	<a href="#" id="logo">
		UNIVERSITAS INDONESIA<br/>
		<em>veritas, probitas, iustitia</em>
	</a>
	
	<h2>Buat akun baru</h2>
	<p>
		For international applicants, please contact our International Office regarding application procedure. This registration form is meant for Indonesian citizen only.
		<br/><br/>Harap menggunakan account lama Anda jika Anda telah membuat pendaftaran sebelumnya.
	</p>
</div>

<div id="tengah" class="form">
	<form action="#">
	
	<h2>Login</h2>
	<table>
		<tr>
			<td>Username <span class="required">*</span></td>
			<td>
				<input type="text" id="username"/>
				<div class="hint">Username yang Anda inginkan (hanya dapat terdiri dari huruf, angka, dan _). Username tidak boleh mengandung spasi.</div>
			</td>
			<td style="color: red; display: none;" id= "td_username">username harus diisi!!!</td>
		</tr>
		<tr>
			<td>Password <span class="required">*</span></td>
			<td>
				<input type="password" id="password"/>
				<div class="hint">Masukkan password. Password minimal terdiri dari 6 karakter dan case sensitive (A beda dengan a).</div>
			</td>
			<td style="color: red; display: none;" id="td_password">password harus diisi!!!</td>
		</tr>
		<tr>
			<td>Ulangi password <span class="required">*</span></td>
			<td><input type="password" id="repassword" onchange="cekSama();"/></td>
			<td style="color: red; display: none;" id="td_repassword">password yang anda masukkan tidak sama!!!</td>
		</tr>
		</table>
		
	<h2>Identitas</h2>
	<table>
		<tr>
			<td> Nama sesuai identitas <span class="required">*</span></td>
			<td>
				<input type="text" id="nama1"/>
				<div class="hint">Nama sesuai yang tertulis di kartu identitas, tanpa gelar akademis/sertifikasi.<br/>
Nama diawali huruf besar di awal dan setelah spasi, contoh benar: Abc Def, contoh salah: abc def, ABC DEF.</div>
			</td>
			<td style="color: red; display: none;" id= "td_nama1">nama sesuai identitas kosong!!!</td>
		</tr>
		<tr>
			<td>Nama sesuai ijazah <span class="required">*</span></td>
			<td>
				<input type="text" id="nama2"/>
				<div class="hint">Nama sesuai yang tertulis di ijazah, tanpa gelar akademis/sertifikasi.<br/>
Nama diawali huruf besar di awal dan setelah spasi, contoh benar: Abc Def, contoh salah: abc def, ABC DEF.</div>
			</td>
			<td style="color: red; display: none;" id= "td_nama2">nama sesuai ijazah kosong!!!</td>
		</tr>
		<tr>
			<td>Jenis identitas <span class="required">*</span></td>
			<td>
				<!-- <select id="jenis_id" onclick="pilihId();"> -->
				<select id="jenis_id">
					<!-- <option value="-">Pilih Jenis Identitas</option> -->
					<option value="ktp">KTP</option>
					<option value="sim">SIM</option>
					<option value="paspor">Paspor</option>
					<option value="kartuPelajar">Kartu Pelajar (berfoto)</option>
				</select>
				<div class="hint">Pilih jenis identitas Anda. Identitas harus dibawa ketika ujian/daftar ulang.</div>
			</td>
			<td style="color: red; display: none;" id= "td_jenisId">pilih jenis identitas!!!</td>
		</tr>
		<tr>
			<td>Nomor identitas <span class="required">*</span></td>
			<td><input type="text" id="nomor_id"/></td>
			<td style="color: red; display: none;" id= "td_nomorId">nomor identitas kosong!!!</td>
		</tr>
		<tr>
			<td>Kewarganegaraan <span class="required">*</span></td>
			<td>
				<select id="kewarganegaraan">
					<option value="Indonesia">Indonesia</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>
				Jenis Kelamin <span class="required">*</span>
			</td>
			<td>
				<input type="radio" name="jk"/> Pria <br/>
				<input type="radio" name="jk"/> Wanita
			</td>
		</tr>
		<tr>
			<td>
				Tanggal lahir <span class="required">*</span>
			</td>
			<td><input type="date" name="bday"/></td>
		</tr>
	</table>

	<h2>Kontak</h2>
	<table>
		<tr>
			<td>Alamat tetap <span class="required">*</span></td>
			<td><input type="text" id="alamatTetap"/></td>
			<td style="color: red; display: none;" id= "td_alamatTetap">alamat tetap kosong!!!</td>
		</tr>
		<tr>
			<td>Negara</td>
			<td>
				<select id="negara">
					<option value="Indonesia">Indonesia</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Provinsi</td>
			<td>
				<select id="provinsi">
					<option value="Jatim">Jawa Timur</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Kabupaten/Kota</td>
			<td>
				<select id="kabkota">
					<option value="Pasuruan">Pasuruan</option>
					<option value="Surabaya">Surabaya</option>
					<option value="Sidoarjo">Sidoarjo</option>
					<option value="Malang">Malang</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Alamat saat ini <span class="required">*</span></td>
			<td><input type="text" id="alamatSekarang"/></td>
			<td style="color: red; display: none;" id= "td_alamatSekarang">alamat saat ini kosong!!!</td>
		</tr>
		<tr>
			<td>Nomor telepon <span class="required">*</span></td>
			<td>
				<input type="text" id="noTelp"/>
				<div class="hint">Nomor telepon yang dapat dihubungi. Pisahkan dengan , (koma) jika ada beberapa nomor.</div>
			</td>
			<td style="color: red; display: none;" id= "td_noTelp">nomor telepon kosong!!!</td>
		</tr>
		<tr>
			<td>No. HP <span class="required">*</span></td>
			<td>
				<input type="text" id="noHP"/>
				<div class="hint">Nomor handphone. Pisahkan dengan , (koma) jika ada beberapa nomor.</div>
			</td>
			<td style="color: red; display: none;" id= "td_noHP">no HP kosong!!!</td>
		</tr>
		<tr>
			<td>Email <span class="required">*</span></td>
			<td>
				<input type="text" id="email"/>
				<div class="hint">Pastikan alamat email Anda benar, email konfirmasi akan dikirim ke alamat email tersebut</div>
			</td>
			<td style="color: red; display: none;" id= "td_email">email kosong!!!</td>
		</tr>
		<tr>
			<td>Ulangi email <span class="required">*</span></td>
			<td><input type="text" id="ulangEmail"/></td>
			<td style="color: red; display: none;" id= "td_ulangEmail">email berbeda!!!</td>
		</tr>
	</table>

	<table>
		<tr>
			<td>
				<button type="button" onclick="cekIsi();">Simpan</button>
			</td>
		</tr>
			<td>
				<div class="hint"><span class="required">*</span> Wajib diisi</div>
			</td>
	</table>
	</form>
</div>

</body>

<script type="text/javascript">
	function cekIsi() {
		var username = document.getElementById("username");
		var td_username = document.getElementById("td_username");
		if (username.value == "") {
			td_username.style.display = "block";
			username.style.borderColor = "red";
		}
		else {
			td_username.style.display = "none";
			username.style.borderColor = "gray";
		}
		
		var nama1 = document.getElementById("nama1");
		var td_nama1 = document.getElementById("td_nama1");
		if (nama1.value == "") {
			td_nama1.style.display = "block";
			nama1.style.borderColor = "red";
		}
		else {
			td_nama1.style.display = "none";
			nama1.style.borderColor = "gray";
		}
		
		var nama2 = document.getElementById("nama2");
		var td_nama2 = document.getElementById("td_nama2");
		if (nama2.value == "") {
			td_nama2.style.display = "block";
			nama2.style.borderColor = "red";
		}
		else {
			td_nama2.style.display = "none";
			nama2.style.borderColor = "gray";
		}
		
		var password = document.getElementById("password");
		var td_password = document.getElementById("td_password");
		if (password.value == "") {
			td_password.style.display = "block";
			password.style.borderColor = "red";
		}
		else {
			td_password.style.display = "none";
			password.style.borderColor = "gray";
		}
		
		var nomor_id = document.getElementById("nomor_id");
		var td_nomorId = document.getElementById("td_nomorId");
		if (nomor_id.value == "") {
			td_nomorId.style.display = "block";
			nomor_id.style.borderColor = "red";
		}
		else {
			td_nomorId.style.display = "none";
			nomor_id.style.borderColor = "gray";
		}
		
		var alamatTetap = document.getElementById("alamatTetap");
		var td_alamatTetap = document.getElementById("td_alamatTetap");
		if (alamatTetap.value == "") {
			td_alamatTetap.style.display = "block";
			alamatTetap.style.borderColor = "red";
		}
		else {
			td_alamatTetap.style.display = "none";
			alamatTetap.style.borderColor = "gray";
		}
		
		var alamatSekarang = document.getElementById("alamatSekarang");
		var td_alamatSekarang = document.getElementById("td_alamatSekarang");
		if (alamatSekarang.value == "") {
			td_alamatSekarang.style.display = "block";
			alamatSekarang.style.borderColor = "red";
		}
		else {
			td_alamatSekarang.style.display = "none";
			alamatSekarang.style.borderColor = "gray";
		}
		
		var noTelp = document.getElementById("noTelp");
		var td_noTelp = document.getElementById("td_noTelp");
		if (noTelp.value == "") {
			td_noTelp.style.display = "block";
			noTelp.style.borderColor = "red";
		}
		else {
			td_noTelp.style.display = "none";
			noTelp.style.borderColor = "gray";
		}
		
		var noHP = document.getElementById("noHP");
		var td_noHP = document.getElementById("td_noHP");
		if (noHP.value == "") {
			td_noHP.style.display = "block";
			noHP.style.borderColor = "red";
		}
		else {
			td_noHP.style.display = "none";
			noHP.style.borderColor = "gray";
		}
		
		var email = document.getElementById("email");
		var td_email = document.getElementById("td_email");
		if (email.value == "") {
			td_email.style.display = "block";
			email.style.borderColor = "red";
		}
		else {
			td_email.style.display = "none";
			email.style.borderColor = "gray";
		}
	}
	
	function cekSama(){
		var password = document.getElementById("password");
		var repassword = document.getElementById("repassword");
		var td_repassword = document.getElementById("td_repassword");
		
		if (password.value!=repassword.value) {
			td_repassword.style.display = "block";
			repassword.style.borderColor = "red";
		} else {
			td_repassword.style.display = "none";
			repassword.style.borderColor = "gray";
		}
		
		var email = document.getElementById("email");
		var ulangEmail = document.getElementById("ulangEmail");
		var td_ulangEmail = document.getElementById("td_ulangEmail");
		
		if (email.value!=ulangEmail.value) {
			td_ulangEmail.style.display = "block";
			ulangEmail.style.borderColor = "red";
		} else {
			td_ulangEmail.style.display = "none";
			ulangEmail.style.borderColor = "gray";
		}
	}
	
	/* function pilihId(){
		var jenis_id = document.getElementById("jenis_id");
		var td_jenisid = document.getElementById("td_jenisId");
		if (jenis_id.value == "-") {
			td_jenisId.style.display = "block";
			jenis_id.style.borderColor = "red";
		}
		else {
			td_jenisId.style.display = "none";
			jenis_id.style.borderColor = "gray";
		}
	} */
	
</script>
</html>